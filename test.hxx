#ifndef TEST_HXX
#define TEST_HXX

#include <cassert>
#include <iostream>
#include <vector>
#include <string>
#include "Utils.hxx"

void assertUtils();
void assertOSName();
void ok();
void info(const char *);

#endif
