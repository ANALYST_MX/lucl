#ifndef SEARCH_HXX
#define SEARCH_HXX

#include <string>
#include "Command.hxx"
#include "OSNameException.hxx"

class Search
{
public:
  Search() throw(OSNameException);
  ~Search();
  void forGoogleQuery(const std::string &);
  void forGoogleImages(const std::string &);
  void forYoutube(const std::string &);
  void forDuckDuckGo(const std::string &);
  void forStackOverflow(const std::string &);
  void forWikiPedia(const std::string &);
private:
  Command *command;
};

#endif
