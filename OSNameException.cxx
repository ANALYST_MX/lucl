#include "OSNameException.hxx"

OSNameException::OSNameException(const char * msg)
{
  text = new char[std::strlen(text) + 1];
  std::strcpy(text, msg);
}

OSNameException::OSNameException(const OSNameException &e)
{
  text = new char[std::strlen(e.text) + 1];
  std::strcpy(text, e.text);
}

const char *OSNameException::what() const noexcept
{
  return text;
}

OSNameException::~OSNameException() throw()
{
  delete[] text;
}
