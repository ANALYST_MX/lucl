#include "Utils.hxx"

std::string Utils::joinVector(const std::vector< std::string > &values, const char delim)
{
  std::string valRes;
  for ( size_t i = 0; i < values.size(); ++i )
    {
      valRes += values[i];
      if (i < values.size() - 1)
	valRes += delim;
    }
  return valRes;
}
