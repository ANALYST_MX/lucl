#include "OSName.hxx"

const char OSName::unknown[] = "unknown";

OSName::OSName() throw(OSNameException)
{
  if ( uname(&name) == -1 )
    {
      throw OSNameException();
    }
}

OSName::~OSName()
{
}

std::string OSName::getKernelName()
{
  return std::string(name.sysname);
}

std::string OSName::getSysName()
{
  return std::string(name.sysname);
}

std::string OSName::getNodeName()
{
  return std::string(name.nodename);
}

std::string OSName::getKernelRelease()
{
  return std::string(name.release);
}

std::string OSName::getRelease()
{
  return std::string(name.release);
}

std::string OSName::getKernelVersion()
{
  return std::string(name.version);
}

std::string OSName::getMachine()
{
  return std::string(name.machine);
}

std::string OSName::getProcessor()
{
  char const *element = unknown;
#if HAVE_SYSINFO && defined SI_ARCHITECTURE
  {
    static char processor[257];
    static size_t s = sizeof processor;
    if ( 0 <= sysinfo(SI_ARCHITECTURE, processor, s) )
      {
	element = processor;
      }
  }
#endif
#ifdef UNAME_PROCESSOR
  if ( element == unknown )
    {
      static char processor[257];
      static size_t s = sizeof processor;
      static int mib[] = { CTL_HW, UNAME_PROCESSOR };
      if ( sysctl(mib, 2, processor, &s, 0, 0) >= 0 )
	{
	  element = processor;
	}
    }
#endif
  return std::string(element);
}

std::string OSName::getHardwarePlatform()
{
  char const *element = unknown;
#if HAVE_SYSINFO && defined SI_PLATFORM
  {
    static char hardware_platform[257];
    static size_t s = sizeof hardware_platform;
    if ( 0 <= sysinfo(SI_PLATFORM, hardware_platform, s) )
      {
	element = hardware_platform;
      }
  }
#endif
#ifdef UNAME_HARDWARE_PLATFORM
  if ( element == unknown )
    {
      static char hardware_platform[257];
      static size_t s = sizeof hardware_platform;
      static int mib[] = { CTL_HW, UNAME_HARDWARE_PLATFORM };
      if ( sysctl(mib, 2, hardware_platform, &s, 0, 0) >= 0 )
	{
	  element = hardware_platform;
	}
    }
#endif
  return std::string(element);
}

std::string OSName::getOperatingSystem()
{
#if defined(WIN) || defined(__WIN32__) || defined(_WIN32) || defined(WIN32) || defined(__WIN32) || defined(__WINDOWS__) || defined(__CYGWIN__) || defined(__CYGWIN32__) || defined(__MINGW32__) || defined(__MINGW__)
  return "Windows 32-bit";
#elif defined(_WIN64)
  return "Windows 64-bit";
#elif defined(__unix) || defined(__unix__) || defined(unix) || defined(UNIX)
  return "Unix";
#elif defined(__APPLE__) || defined(__MACH__) || defined(Macintosh) || defined(macintosh)
  return "Mac OSX";
#elif defined(__gnu_linux__) || defined(__linux__) || defined(__linux_) || defined(LINUX) || defined(linux)
  return "GNU/Linux";
#elif defined(__posix__)
  return "Posix"
#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
  return "FreeBSD";
#elif defined(__sun__) || defined(__sun) || defined(__SUNPRO_CC) || defined(__SunOS_OSversion)
  return "SunOS";
#elif defined(__SVR4)
  return "Solaris";
#elif defined(__hpux)
  return "HP-UX";
#elif defined(__osf__)
  return "OSF/1";
#elif defined(__ultrix)
  return "Ultrix";
#elif defined(_AIX)
  return "AIX";
#elif defined(__sgi__)|| defined(sgi) || defined(__sgi)
  return "SGI";
#elif defined(__QNX__)
  return "QNX";
#elif defined(__DragonFly__)
  return "DragonFly";
#elif defined(__OpenBSD__)
  return "OpenBSD";
#elif defined(__NetBSD__)
  return "NetBSD";
#elif defined(__bsdi__)
  return "BSD/OS";
#elif defined(__NeXT__)
  return "NEXT";
#else
  return "Other";
#endif
}
