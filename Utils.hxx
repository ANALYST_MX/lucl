#ifndef UTILS_HXX
#define UTILS_HXX

#include <string>
#include <vector>

class Utils
{
public:
  std::string joinVector(const std::vector< std::string > &, const char);
};

#endif
