#ifndef COMMAND_HXX
#define COMMAND_HXX

#include <string>
#include <cstdlib>
#include "OSName.hxx"
#include "OSNameException.hxx"

class Command
{
public:
  Command() throw(OSNameException);
  Command(const std::string &);
  ~Command();
  void open(const std::string &);
private:
  std::string defaultApplication;
};

#endif
