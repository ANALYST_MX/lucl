GXX := g++
GXXFLAG := -std=c++11 -Wall -g
MAIN := main
TEST := test
CXX := .cxx
HXX := .hxx
OBJ := .o
OBJ_LIST := OSName$(OBJ) OSNameException$(OBJ) Command$(OBJ) Search$(OBJ) Utils$(OBJ)
LIB_LIST := -lboost_program_options
all: $(MAIN) $(TEST)
$(MAIN): $(MAIN)$(OBJ) $(OBJ_LIST)
	$(GXX) $(GXXFLAG) $(MAIN)$(OBJ) $(OBJ_LIST) -o $(MAIN) $(LIB_LIST)
$(MAIN)$(OBJ): $(MAIN)$(CXX) $(MAIN)$(HXX)
	$(GXX) $(GXXFLAG) -c $(MAIN)$(CXX) -o $(MAIN)$(OBJ) $(LIB_LIST)
$(TEST): $(TEST)$(OBJ) $(OBJ_LIST)
	$(GXX) $(GXXFLAG) $(TEST)$(OBJ) $(OBJ_LIST) -o $(TEST)
$(TEST)$(OBJ): $(TEST)$(CXX) $(TEST)$(HXX)
	$(GXX) $(GXXFLAG) -c $(TEST)$(CXX) -o $(TEST)$(OBJ)
OSName$(OBJ): OSName$(CXX) OSName$(HXX)
	$(GXX) $(GXXFLAG) -c OSName$(CXX) -o OSName$(OBJ)
OSNameException$(OBJ): OSNameException$(CXX) OSNameException$(HXX)
	$(GXX) $(GXXFLAG) -c OSNameException$(CXX) -o OSNameException$(OBJ)
Command$(OBJ): Command$(CXX) Command$(HXX)
	$(GXX) $(GXXFLAG) -c Command$(CXX) -o Command$(OBJ)
Search$(OBJ): Search$(CXX) Search$(HXX)
	$(GXX) $(GXXFLAG) -c Search$(CXX) -o Search$(OBJ)
Utils$(OBJ): Utils$(CXX) Utils$(HXX)
	$(GXX) $(GXXFLAG) -c Utils$(CXX) -o Utils$(OBJ)
.PHONY: clean check
clean:
	rm -rf $(MAIN) $(TEST) *$(OBJ) *.out *~
check:
	./$(TEST)
