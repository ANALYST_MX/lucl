#include "Search.hxx"

Search::Search() throw (OSNameException)
  : command(new Command)
{
}

Search::~Search()
{
  delete command;
}

void Search::forGoogleQuery(const std::string &query)
{
  std::string url = "https://www.google.com/search?q=" + query;
  command->open(url);
}

void Search::forGoogleImages(const std::string &query)
{
  std::string url = "https://www.google.com/search?q=" + query + "&tbm=isch";
  command->open(url);
}

void Search::forYoutube(const std::string &query)
{
  std::string url = "https://www.youtube.com/results?search_query=" + query;
  command->open(url);
}

void Search::forDuckDuckGo(const std::string &query)
{
  std::string url = "https://duckduckgo.com/?q=" + query;
  command->open(url);
}

void Search::forStackOverflow(const std::string &query)
{
  std::string url = "http://stackoverflow.com/search?q=" + query;
  command->open(url);
}

void Search::forWikiPedia(const std::string &query)
{
  std::string url = "http://www.wikipedia.org/w/index.php?search=" + query;
  command->open(url);
}
