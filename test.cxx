#include "test.hxx"

int main(const int argc, const char *argv[])
{
  assertUtils();
  
  return EXIT_SUCCESS;
}

void assertUtils()
{
  std::cout << "\t\033[1;34mstart check Utils\033[0m" << std::endl;
  Utils u;
  info("check joinVector");
  std::vector<std::string> v;
  v.push_back("a");
  v.push_back("b");
  v.push_back("c");
  assert(u.joinVector(v, ' ') == "a b c");
  ok();
  std::cout << "\t\033[1;34mend check Utils\033[0m" << std::endl;
}

void ok()
{
  std::cout << "\033[1;32mok\033[0m" << std::endl;
}

void info(const char *msg)
{
  std::cout << msg << ": ";
}
