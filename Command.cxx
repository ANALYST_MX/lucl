#include "Command.hxx"

Command::Command() throw(OSNameException)
{
  if ( OSName().getKernelName() == "Darwin" )
    {
      defaultApplication = "open";
    }
  
  defaultApplication = "xdg-open";
}

Command::Command(const std::string &app)
{
  defaultApplication = app;
}

Command::~Command()
{
}

void Command::open(const std::string &path)
{
  std::string command = defaultApplication + " " + path;
  system(command.c_str());
}
