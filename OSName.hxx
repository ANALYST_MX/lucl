#ifndef OS_NAME_HXX
#define OS_NAME_HXX

#include <string>
#include <sys/utsname.h>
#include "OSNameException.hxx"

#if HAVE_SYSINFO && HAVE_SYS_SYSTEMINFO_H
# include <sys/systeminfo.h>
#endif

#if HAVE_SYSCTL && HAVE_SYS_SYSCTL_H
# include <sys/param.h> /* needed for OpenBSD 3.0 */
# include <sys/sysctl.h>
# ifdef HW_MODEL
#  ifdef HW_MACHINE_ARCH
/* E.g., FreeBSD 4.5, NetBSD 1.5.2 */
#   define UNAME_HARDWARE_PLATFORM HW_MODEL
#   define UNAME_PROCESSOR HW_MACHINE_ARCH
#  else
/* E.g., OpenBSD 3.0 */
#   define UNAME_PROCESSOR HW_MODEL
#  endif
# endif
#endif

class OSName
{
public:
  OSName() throw(OSNameException);
  ~OSName();
  std::string getKernelName();
  std::string getSysName();
  std::string getNodeName();
  std::string getKernelRelease();
  std::string getRelease();
  std::string getKernelVersion();
  std::string getMachine();
  std::string getProcessor();
  std::string getHardwarePlatform();
  std::string getOperatingSystem();
private:
  struct utsname name;
  static char const unknown[];
};

#endif
