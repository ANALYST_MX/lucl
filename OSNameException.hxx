#ifndef OS_NAME_EXCEPTION_HXX
#define OS_NAME_EXCEPTION_HXX

#include <exception>
#include <cstring>

class OSNameException : public std::exception
{
public:
  OSNameException(const char * = "cannot get system name");
  OSNameException(const OSNameException &);
  ~OSNameException() throw();
  virtual const char *what() const noexcept;
private:
  char *text;
};

#endif
