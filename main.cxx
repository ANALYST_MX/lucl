#include "main.hxx"

int main(int argc, char *argv[])
{
  const char delim = ' ';
  po::positional_options_description pd;
  pd.add("query", -1);

  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("query,q", po::value< std::vector< std::string > >()->multitoken(), "simple google search")
    ("images,i", po::value< std::vector< std::string > >()->multitoken(), "google images search")
    ("youtube,y", po::value< std::vector< std::string > >()->multitoken(), "youtube search")
    ("stack,s", po::value< std::vector< std::string > >()->multitoken(), "search stackoverflow")
    ("wiki,w", po::value< std::vector< std::string > >()->multitoken(), "search wikipedia")
    ("ddg,d", po::value< std::vector< std::string > >()->multitoken(), "search duckduckgo")
    ;
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  po::notify(vm);

  try
    {
      Search s;
      Utils u;
      if ( vm.count("query") )
	{
	  s.forGoogleQuery(u.joinVector(vm["query"].as< std::vector< std::string> >(), delim));
	}  
      else if ( vm.count("images") )
	{
	  s.forGoogleImages(u.joinVector(vm["images"].as< std::vector< std::string> >(), delim));
	}
      else if ( vm.count("youtube") )
	{
	  s.forYoutube(u.joinVector(vm["youtube"].as< std::vector< std::string> >(), delim));
	}
      else if ( vm.count("stack") )
	{
	  s.forStackOverflow(u.joinVector(vm["stack"].as< std::vector< std::string> >(), delim));
	}
      else if ( vm.count("wiki") )
	{
	  s.forWikiPedia(u.joinVector(vm["wiki"].as< std::vector< std::string> >(), delim));
	}
      else if ( vm.count("ddg") )
	{
	  s.forDuckDuckGo(u.joinVector(vm["ddg"].as< std::vector< std::string> >(), delim));
	}
      else
	{
	  std::cout << desc << std::endl;
	}
    }
  catch (std::exception &e)
    {
      std::cerr << e.what() << std::endl;
      return EXIT_FAILURE;
    }
  
  return EXIT_SUCCESS;
}
